import { Tile } from './tile';
import { Participant } from './participant';
import { TileSet } from './tile-set';

export interface LastAction {
  type: string;
  account_id: number;
}

export interface Game {
  status: string;
  created_at: string;
  upstringd_at: string;
  started_at: string;
  ended_at: string;
  creator_account_id: number;
  active_account_id: number;
  winner_account_id: number;
  move_start_timestamp: string;
  tile_pool_size: number;
  tile_rack: Tile[];
  chat_count: number;
  chat_unread_count: number;
  last_action: LastAction;
  participants: Participant[];
  queue_id: string;
  board: TileSet[];
  uri: string;
}
