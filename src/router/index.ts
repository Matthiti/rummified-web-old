import Vue from 'vue';
import VueRouter, { RouteConfig, Route, NavigationGuardNext } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '',
    component: () => import(/* webpackChunkName: "default" */ '../layouts/DefaultLayout.vue'),
    children: [
      {
        path: '/',
        name: 'games',
        component: () => import(/* webpackChunkName: "games" */ '../views/Games.vue')
      }
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to: Route, from: Route, next: NavigationGuardNext<Vue>) => {
  if (Vue.prototype.$http.isLoggedIn || to.name === 'login') {
    return next();
  }
  return next({ name: 'login' });
});

export default router;
