module.exports = {
  devServer: {
    proxy: {
      '^/api': {
        target: 'https://www.rummified.com'
      }
    }
  },
  css: {
    extract: true,
    loaderOptions: {
      sass: {
        prependData: '@import "@/assets/scss/_bootstrap-required.scss";'
      }
    }
  }
};
