import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { HttpService } from './services/http.service';

Vue.config.productionTip = false;

Vue.prototype.$http = new HttpService();

declare module 'vue/types/vue' {
  interface Vue {
    $http: HttpService;
  }
}

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
